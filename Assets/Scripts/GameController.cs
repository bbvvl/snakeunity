﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject snakePrefab;
    public Snake head;
    public Snake tail;

    public int NESW;
    public float speed = 0.15f;//0.05f top speed:)

    public Vector2 nexPos;

    public int maxSize;
    public int currentSize;

    public int x;
    public int y;
    public GameObject foodPrefab;
    public GameObject currentFood;

    public int score;
    public Text scoreText;

    int dif = 1;
    int swipeEn = 1;
    int hitEn = 1;

    public Swipe swipe;


    private void OnEnable()
    {
        Snake.hit += hit;
    }

    private void OnDisable()
    {
        Snake.hit -= hit;
    }

    void Start()
    {
        swipeEn += PlayerPrefs.GetInt("SwipesEnabled");
        hitEn += PlayerPrefs.GetInt("HitsEnabled");
        dif = PlayerPrefs.GetInt("Dificult");
        switch (dif)
        {
            case 1:
                speed = 0.25f;
                break;
            case 2:
                speed = 0.15f;
                break;
            case 3:
                speed = 0.05f;
                break;
            case 4:
                speed = 0.04f;
                break;
        }
        scoreText.text = score.ToString();
        ChangeFood();
        InvokeRepeating("TimerInvoke", 0, speed);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }
        AndroidChangeDirectionWithSwipes();
        DesktopChangeDirection();
    }

    void TimerInvoke()
    {
        Movement();
        StartCoroutine(CheckVisible());
        if (currentSize >= maxSize)
        {
            ChangeTail();
        }
        else
        {
            currentSize++;
        }
    }

    void Movement()
    {
        GameObject temp;

        nexPos = head.transform.position;

        switch (NESW)
        {
            case 0:
                nexPos = new Vector2(nexPos.x, nexPos.y + 1);
                break;
            case 1:
                nexPos = new Vector2(nexPos.x + 1, nexPos.y);
                break;
            case 2:
                nexPos = new Vector2(nexPos.x, nexPos.y - 1);
                break;
            case 3:
                nexPos = new Vector2(nexPos.x - 1, nexPos.y);
                break;

        }

        temp = (GameObject)Instantiate(snakePrefab, nexPos, transform.rotation);
        //СОЗДАЕМ СВЯЗЬ ГОЛОВЫ СО СЛЕД ЭЛЕМЕНТОМ И СТАВИМ НА МЕСТО ГОЛОВЫ СЛЕДУЮЩИЙ
        head.setNext(temp.GetComponent<Snake>());
        head = temp.GetComponent<Snake>();

        return;
    }

    void DesktopChangeDirection()
    {
        if (Input.GetKeyDown(KeyCode.W) && NESW != 2)
        {
            NESW = 0;
        }

        if (Input.GetKeyDown(KeyCode.S) && NESW != 0)
        {
            NESW = 2;
        }
        if (Input.GetKeyDown(KeyCode.A) && NESW != 1)
        {
            NESW = 3;
        }
        if (Input.GetKeyDown(KeyCode.D) && NESW != 3)
        {
            NESW = 1;
        }
    }

    public void AndroidChangeDirection(int dir)//0 left 1- right
    {
        if (swipeEn == 1 && dif != 4)
        {
            //nesw 0 up 1 right 2 down 3 left
            if (dir == 0)
            {
                NESW--;
                if (NESW < 0) NESW += 4;
            }
            else
            {
                NESW++;
                if (NESW > 3) NESW -= 4;
            }
        }
    }

    public void AndroidChangeDirectionWithSwipes()
    {

        if (swipeEn == 2 || dif == 4)
        {            //nesw 0 up 1 right 2 down 3 left

            if (swipe.SwipeLeft && NESW != 1)
                NESW = 3;
            if (swipe.SwipeUp && NESW != 2)
                NESW = 0;
            if (swipe.SwipeDown && NESW != 0)
                NESW = 2;
            if (swipe.SwipeRight && NESW != 3)
                NESW = 1;
        }
    }

    void ChangeTail()
    {
        Snake tempSnake = tail;
        tail = tail.getNext();
        tempSnake.removeTail();
    }

    void ChangeFood()
    {
        int xPos = Random.Range(-x, x);
        int yPos = Random.Range(-y, y);

        currentFood = (GameObject)Instantiate(foodPrefab, new Vector2(xPos, yPos), transform.rotation);
        StartCoroutine(CheckRender(currentFood));
    }

    IEnumerator CheckRender(GameObject IN)
    {
        yield return new WaitForEndOfFrame();

        if (!IN.GetComponent<Renderer>().isVisible)
        {
            if (IN.tag == "Food")
            {
                Destroy(IN);
                ChangeFood();
            }
        }
    }

    IEnumerator CheckVisible()
    {
        yield return new WaitForEndOfFrame();

        if (!head.GetComponent<Renderer>().isVisible)
        {
            if (dif == 4 || hitEn != 1) { hit("Snake"); }
            else
                wrap();
        }
    }


    void hit(string IN)
    {
        if (IN == "Food")
        {
            ChangeFood();
            maxSize++;
            if (dif == 4) { score += 16; }
            else
                score += 1 * dif * swipeEn * hitEn;
            scoreText.text = score.ToString();

            if (speed > 0.05f)
            {
                CancelInvoke("TimerInvoke");
                speed -= 0.01f;//0.25 -0.05-0.2
                InvokeRepeating("TimerInvoke", 0, speed);
            }

        }




        if (IN == "Snake")
        {
            CancelInvoke("TimerInvoke");

            if (score > PlayerPrefs.GetInt("HS"))
            {
                PlayerPrefs.SetInt("HS", score);
            }
            PlayerPrefs.SetInt("LS", score);
            Exit();
        }
    }



    public void Exit()
    {
        SceneManager.LoadScene(0);
    }

    void wrap()
    {
        if (NESW == 0)
        {
            head.transform.position = new Vector2(head.transform.position.x, -(head.transform.position.y - 1));
        }
        else
        if (NESW == 1)
        {
            head.transform.position = new Vector2(-(head.transform.position.x - 1), head.transform.position.y);

        }
        else
        if (NESW == 2)
        {
            head.transform.position = new Vector2(head.transform.position.x, -(head.transform.position.y + 1));

        }
        else
        if (NESW == 3)
        {
            head.transform.position = new Vector2(-(head.transform.position.x + 1), head.transform.position.y);
        }

    }
}
