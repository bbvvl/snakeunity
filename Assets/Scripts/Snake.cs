﻿using System;
using UnityEngine;

public class Snake : MonoBehaviour
{

    private Snake next;

    static public Action<String> hit;

    public void setNext(Snake IN)
    {
        next = IN;
    }

    public Snake getNext()
    {
        return next;
    }

    public void removeTail()
    {
        Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (hit != null)
        {
            hit(other.tag);
        }

        if (other.tag == "Food")
        {
            Destroy(other.gameObject);
        }
    }
}
