﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{

    public Toggle swipes;
    public Toggle hits;
    public Toggle difficult_1;
    public Toggle difficult_2;
    public Toggle difficult_3;
    public Toggle difficult_4;

    // Use this for initialization
    void Start()
    {
        swipes.isOn = PlayerPrefs.GetInt("SwipesEnabled") % 2 == 1;
        hits.isOn = PlayerPrefs.GetInt("HitsEnabled") % 2 == 1;
        switch (PlayerPrefs.GetInt("Dificult"))
        {
            case 1:
                difficult_1.isOn = true;
                break;
            case 2:
                difficult_2.isOn = true;
                break;
            case 3:
                difficult_3.isOn = true;
                break;
            case 4:
                difficult_4.isOn = true;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }
    }

    public void setSwipes()
    {

        if (swipes.isOn)
        {
            PlayerPrefs.SetInt("SwipesEnabled", 1);
        }
        else
        {
            PlayerPrefs.SetInt("SwipesEnabled", 0);
        }

    }

    public void setHits()
    {

        if (hits.isOn)
        {
            PlayerPrefs.SetInt("HitsEnabled", 1);
        }
        else
        {
            PlayerPrefs.SetInt("HitsEnabled", 0);
        }

    }

    public void setDifficult(int dif)
    {

        if (dif == 1 && difficult_1.isOn)
        {
            difficult_2.isOn = false;
            difficult_3.isOn = false;
            difficult_4.isOn = false;
            PlayerPrefs.SetInt("Dificult", dif);
        }

        else
        if (dif == 2 && difficult_2.isOn  )
        {
            difficult_1.isOn = false;
            difficult_3.isOn = false;
            difficult_4.isOn = false;
            PlayerPrefs.SetInt("Dificult", dif);

        }
        else
            if (dif == 3 && difficult_3.isOn  )
        {
            difficult_2.isOn = false;
            difficult_1.isOn = false;
            difficult_4.isOn = false;
            PlayerPrefs.SetInt("Dificult", dif);

        }
        else
            if (dif == 4 && difficult_4.isOn)
        {
            difficult_2.isOn = false;
            difficult_3.isOn = false;
            difficult_1.isOn = false;
            PlayerPrefs.SetInt("Dificult", dif);

        }
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
    }
}
