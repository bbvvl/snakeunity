﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{

    public Text hs;
    public Text ls;

    void Start()
    {
        HighScore();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Settings()
    {
        SceneManager.LoadScene(2);

    }

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        Application.Quit();
    }

    void HighScore()
    {
        hs.text = hs.text.Substring(0, 12) + PlayerPrefs.GetInt("HS").ToString();
        ls.text = ls.text.Substring(0, 12) + PlayerPrefs.GetInt("LS").ToString();
    }
}
